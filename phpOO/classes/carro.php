<?php

class Carro {

    private $fabricante;
    private $modelo;
    private $marca;
    private $cor;
    private $combustivel;
    private $placa;

    public function setModelo($modeloparam) {
        $this->modelo = $modeloparam;
    }

    public function setMarca($marcaparam) {
        $this->marca = $marcaparam;
    }

    public function setCor($corparam) {
        $this->cor = $corparam;
    }

    public function setCombustivel($combustivelparam) {
        $this->combustivel = $combustivelparam;
    }

    public function setPlaca($placaparam) {
        $this->placa = $placaparam;
    }

    public function getModelo() {
        echo 'Modelo: ';
        echo $this->modelo;
    }

    public function getMarca() {
        echo 'Marca: ';
        echo $this->marca;
    }

    public function getCor() {
        echo 'Cor: ';
        echo $this->cor;
    }

    public function getCombustivel() {
        echo 'Combustivel: ';
        echo $this->combustivel;
    }

    public function getPlaca() {
        echo 'Placa: ';
        echo $this->placa;
    }

}

?>